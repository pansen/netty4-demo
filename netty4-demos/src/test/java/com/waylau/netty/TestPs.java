package com.waylau.netty;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author pansen
 * @date 2018/2/24
 * desc:
 **/

public class TestPs {
    /*@Test
    public void test() {
        int man = 0;
        int female = 0;
        int same = 0;
        int country = 0;
        while (country<10000){
            Map<String, Integer> morFbyCountry = getMorFbyCountry();
            if(morFbyCountry.get("man")>morFbyCountry.get("female")){
                man++;
            }else if(morFbyCountry.get("man")<morFbyCountry.get("female")){
                female++;
            }else {
                same++;
            }
            country++;
        }
        System.out.println("man = " + man);
        System.out.println("female = " + female);
        System.out.println("same = " + same);
        System.out.println("man%total = " + (double)female/(double)(country-same)*100+"%");
    }*/

    @Test
    public void name() {
        double manSum = 0;
        for(int i =0;i<10;i++){
            manSum+=getMorFbyCountry();
        }
        double v = manSum / (10);
        System.out.println("v = " + v);
    }


    public double getMorFbyCountry() {
        Map<String, Integer> map= new HashMap<>();
        map.put("man",0);
        map.put("female",0);
        for(int i =0;i<100000000;i++){
            Map<String, Integer> morF = getMorF();
            map.put("man",map.get("man")+morF.get("man"));
            map.put("female",map.get("female")+morF.get("female"));
        }
        double result = (double) map.get("man") / (map.get("man") + map.get("female"));
        System.out.println("man%total = " + (double)map.get("man")/(map.get("man")+map.get("female"))*100+"%");
        return result;
    }


    /**
     * random 0 for man ,1 fro female
     */

    public Map<String, Integer> getMorF() {
        Map<String, Integer> hashMap = new HashMap<>();
        int man = 0;
        int female = 0;
        Random random = new Random();
        while (true) {
            int i = random.nextInt(2);
            if(i==0){
                man++;
                break;
            }
            female++;
        }
        hashMap.put("man",man);
        hashMap.put("female",female);
        return hashMap;
    }
}
