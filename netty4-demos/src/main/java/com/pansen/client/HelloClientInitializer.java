package com.pansen.client;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpRequestEncoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @author pansen
 * @date 2018/2/23
 * desc:
 **/

public class HelloClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {

        socketChannel.pipeline()
//                .addLast("framer", new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()))
                /*.addLast("decoder", new StringDecoder())
                .addLast("encoder", new StringEncoder())*/
        .addLast("decoder", new HttpRequestDecoder())
        .addLast("encoder", new HttpResponseEncoder())
                .addLast("1",new HelloClientHandler());
    }
}
